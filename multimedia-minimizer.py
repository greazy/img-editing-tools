#!/usr/bin/env python
import os
from PIL import Image

midSize = 500, 500
smallSize = 75, 75
inputDir = "photos"
outputDir = "photos_output"


def save_image(infile, outfile, size, quality, need_fit=False):
    try:
        image = Image.open(infile)

        if need_fit:
            square_size = (min(image.size), min(image.size))
            background = Image.new('RGBA', square_size, (255, 255, 255, 0))
            background.paste(image, (0, 0))
            image = background

        image.thumbnail(size, Image.ANTIALIAS)

        image.save(outfile, "jpeg", quality=quality, optimize=True, progressive=True)
    except Exception as e:
        print("cannot create thumbnail for '%s'. %s" % (infile, e.strerror))


for fileName in os.listdir(inputDir):
    infile = os.path.join(inputDir, fileName)

    midOutfile = os.path.splitext(os.path.join(outputDir, fileName))[0] + "_m.jpg"
    smallOutfile = os.path.splitext(os.path.join(outputDir, fileName))[0] + "_s.jpg"

    save_image(infile, smallOutfile, smallSize, 90, True)
    save_image(infile, midOutfile, midSize, 90)
