#!/usr/bin/env python
import os
from PIL import Image

inputDir = "photos"
outputDir = "photos_output"


def save_image(infile, outfile, quality):
    try:
        image = Image.open(infile)

        image.save(outfile, "jpeg", quality=quality, optimize=True)
    except Exception as e:
        print("cannot create thumbnail for '%s'. %s" % (infile, e.strerror))


for fileName in os.listdir(inputDir):
    infile = os.path.join(inputDir, fileName)

    outfile = os.path.join(outputDir, fileName)

    save_image(infile, outfile, 85)
